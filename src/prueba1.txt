pruebas sobre sumar
SUMAR_DOS_NUMEROS(10,5) => 15
SUMAR_DOS_NUMEROS(0,5) => 5
SUMAR_DOS_NUMEROS(5,10) => 15
SUMAR_DOS_NUMEROS(-5,10) => 5

pruebas sobre restar
SUMAR(10,5) => 5
SUMAR(0,5) => 0
SUMAR(5,10) => -5
SUMAR(-5,10) => -15

pruebas sobre multiplicar
SUMAR(10,5) => 500
SUMAR(0,5) => 0
SUMAR(5,10) => 500
SUMAR(-5,10) => -500